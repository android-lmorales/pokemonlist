package com.lmorales.pokedex;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.lmorales.pokedex.models.Pokemon;

import java.util.ArrayList;
import java.util.List;

public class ListaPokemonAdapter extends RecyclerView.Adapter<ListaPokemonAdapter.ViewHolder> {
    //creamos listado de Pokemon
    private ArrayList<Pokemon> dataset;
    // usamos inflater para indicar que view se utilizara
    private LayoutInflater mInflater;
    //
    private Context context;

    public ListaPokemonAdapter(ArrayList<Pokemon> dataset,Context context) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
        this.dataset = dataset;
    }

    public ListaPokemonAdapter(Context context){
        this.context = context;
        this.dataset = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view = mInflater.inflate(R.layout.item_pokemon,null);

    return new ViewHolder(view);
  }
  @Override
  public void onBindViewHolder(ViewHolder holder, int position){
      Pokemon p = dataset.get(position);
      holder.nombreTextView.setText(p.getName());
      Glide.with(context).load("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/"+p.getNumber()+".png")
              .centerCrop()
              .diskCacheStrategy(DiskCacheStrategy.ALL)
              .into(holder.fotoImageView);
  }
  @Override
  public int getItemCount(){
        return dataset.size();
  }

    public void adicionarListaPokemon(ArrayList<Pokemon> listaPokemon) {
        for (int i = 0; i < listaPokemon.size(); i++) {
            Pokemon p = listaPokemon.get(i);
            Log.i("TAG","Pokemon "+p.getName());
            Log.i("TAG",p.getUrl());
        }
        dataset.addAll(listaPokemon);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView fotoImageView;
        private TextView nombreTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            fotoImageView = (ImageView) itemView.findViewById(R.id.fotoImageView);
            nombreTextView = (TextView) itemView.findViewById(R.id.nombreTextView);
        }
    }
}
