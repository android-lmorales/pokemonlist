package com.lmorales.pokedex;

public class Paginate {
    private int limit;
    private int offset;
    private int datasize;



    public Paginate(int limit, int offset, int datasize) {
        this.limit = limit;
        this.offset = offset;
        this.datasize = datasize;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getDatasize() {
        return datasize;
    }

    public void setDatasize(int datasize) {
        this.datasize = datasize;
    }
}
