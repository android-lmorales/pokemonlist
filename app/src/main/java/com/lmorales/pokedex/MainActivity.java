package com.lmorales.pokedex;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;

import com.lmorales.pokedex.models.Pokemon;
import com.lmorales.pokedex.models.PokemonRespuesta;
import com.lmorales.pokedex.pokeapi.PokeapiService;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "POKEDEX";

    private Retrofit retrofit;
    private RecyclerView recyclerView;
    private ListaPokemonAdapter listaPokemonAdapter;
    private ArrayList<Pokemon> elements;
    private Context context;
    private Paginate paginate;
    private TextView textViewDesde;
    private TextView textViewHasta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.context = this;
        this.paginate = new Paginate(15,1,15);
        textViewDesde = (TextView) findViewById(R.id.textViewDesde);
        textViewHasta = (TextView) findViewById(R.id.textViewHasta);
        textViewDesde.setText(String.valueOf(1));
        textViewHasta.setText(String.valueOf(15));

        retrofit = new Retrofit.Builder()
                .baseUrl("https://pokeapi.co/api/v2/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Button btnAnterior = (Button) findViewById(R.id.btnAnterior);
        btnAnterior.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.i(TAG,"Anterior");
                int resta = paginate.getOffset()-paginate.getDatasize();
                textViewHasta.setText(String.valueOf(paginate.getOffset()));
                textViewDesde.setText(String.valueOf(resta));
                paginate.setOffset(resta);
                datosPokemon();

            }
        });
        Button btnSiguiente = (Button) findViewById(R.id.btnSiguiente);
        btnSiguiente.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.i(TAG,"Siguiente");
                Log.i(TAG,String.valueOf(paginate.getOffset()-paginate.getDatasize()));
                Log.i(TAG,String.valueOf(paginate.getLimit()-paginate.getDatasize()));
                int suma = paginate.getOffset()+paginate.getDatasize();
                textViewHasta.setText(String.valueOf(suma-1+paginate.getDatasize()));
                textViewDesde.setText(String.valueOf(suma-1));
                paginate.setOffset(suma);
                datosPokemon();

            }
        });
        datosPokemon();
    }
     private void datosPokemon(){

        PokeapiService service = retrofit.create(PokeapiService.class);
         Log.i("offset",String.valueOf(this.paginate.getOffset()));
         Log.i("limit",String.valueOf(this.paginate.getLimit()));

         Call<PokemonRespuesta> pokemonRespuestaCall = service.obtenerListaPokemon(this.paginate.getOffset(),this.paginate.getLimit());
        pokemonRespuestaCall.enqueue(new Callback<PokemonRespuesta>() {
            @Override
            public void onResponse(Call<PokemonRespuesta> call, Response<PokemonRespuesta> response) {
                if(response.isSuccessful())
               {
                   PokemonRespuesta pokemonRespuesta = response.body();
                   ArrayList<Pokemon> listaPokemon = pokemonRespuesta.getResults();
                   listaPokemonAdapter = new ListaPokemonAdapter(listaPokemon,context);
                   recyclerView =(RecyclerView) findViewById(R.id.recyclerView);
                   recyclerView.setAdapter(listaPokemonAdapter);
                   recyclerView.setHasFixedSize(true);
                   GridLayoutManager gridLayoutManager = new GridLayoutManager(context,3);
                   recyclerView.setLayoutManager(gridLayoutManager);
               }else{
                   Log.e(TAG,"onResponse "+response.errorBody());
               }
            }

            @Override
            public void onFailure(Call<PokemonRespuesta> call, Throwable t) {
                Log.e(TAG,"onFailure "+t.getMessage());
            }
        });
    }
}